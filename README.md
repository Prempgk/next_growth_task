#  <a href="https://nextgrowthlabs.com/"><img src="https://nextgrowthlabs.com/wp-content/uploads/2021/11/nextlogo.jpeg" margin-right='10px' width='30' height='30' alt="Next Labs" id="logo"></a> Next Growth's Django Evaluation Task for Python Developer
<a class="header-badge" target="_blank" href="https://docs.google.com/document/d/1um-TSfZV25HJAty7B-lUGf0lBbUfBqQ-qoM78_-1cjA/edit#">
  <img src="https://img.shields.io/badge/style--5eba00.svg?label=Click here for Task details&style=social">
</a>

##Task 1

###--> *Preview* :

    To extract some values from the given input using regex method

##Task 2

    Scalable Django web application with admin & user authentication to add & download android applications

--> *Preview :*

## The Lomofy Diagram
<br>
<div align="center">
  <a href="https://drawsql.app/teams/prems-team/diagrams/ngl-task2-data-structure">
    <img width="80%" align="center" src="Task2/static/images/sql_structure.png"/>
  </a>
</div>

--> *Full View:*

You can see clearly the diagram at :&nbsp; <a href="https://drawsql.app/teams/prems-team/diagrams/ngl-task2-data-structure">
<img align="center" src="https://img.shields.io/badge/Lomofy-SQL%20Diagram%20-orange"></a>

##Introduction

This is a web application for Android app management. 
Admin can add the app & user can download it & earn points by complete every task. 

## 📸 Screenshot
<details>
 <summary>
    🔎 View
 </summary>

 ### 💻 Signin
![signin](Task2/static/images/signin.png)

### ➡️ Signup
![signup](Task2/static/images/signup.png)

### ⌛ Admin home
![Admin home](Task2/static/images/admin_homepage.png)

 ### ➕ Add application
 ![Add application](Task2/static/images/Add_product.png)

 
 ### 👨🏻‍💻 User Profile
 ![user-profile](Task2/static/images/profile.png)

### 🛒 User Home
![user-home](Task2/static/images/user_home.png)

### 💻 tasks
![tasks](Task2/static/images/task.png)

### ✅ points
![points](Task2/static/images/points.png)

</details>





## Table Of Contents:

- [💻 Application Overview](Task2/static/docs/application-overview.md)
- [⚙   Project Configuration](Task2/static/docs/project-configuration.md)
- [📚 Additional Resources](Task2/static/docs/additional-resources.md)


## Task 3:

-->Preview:

    Content writing about periodic task scheduling, Django & Flask usages.

### Refer:

- [Periodic Task schedulers](Task3/a.md)
- [Django, Flask usages](Task3/b.md)