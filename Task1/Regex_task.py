import re


def regex_task(data):
    patterns = ['id', 'code']
    extract_dict_list = []
    for i in data:
        dict_values = data[i]
        for j in dict_values:
            dicti = j
            regex_matches = [re.compile("^" + pattern + "$").match for pattern in patterns]
            for k, v in dicti.items():
                for rg in regex_matches:
                    if rg(k):
                        extract_dict_list.append(v)
    return extract_dict_list


input_dict = {
    "orders": [{"id": 1}, {"id": 2}, {"id": 3}, {"id": 4}, {"id": 5}, {"id": 6}, {"id": 7}, {"id": 8}, {"id": 9},
               {"id": 10}, {"id": 11}, {"id": 648}, {"id": 649}, {"id": 650}, {"id": 651}, {"id": 652},
               {"id": 653}],
    "errors":
        [{"code": 3,
          "message": "[PHP Warning #2] count(): Parameter must be an array or an object that implements "
                     "Countable (153)"
          }]}

print(regex_task(input_dict))
