from django.apps import AppConfig


class NgtAdminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ngt_admin'
