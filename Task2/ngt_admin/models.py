from django.apps import apps
from django.contrib import auth
from django.contrib.auth.hashers import make_password
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.exceptions import ValidationError
from django.db import models
# Create your models here.
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager  # custom model
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


# custom manager for our custom user model You should also define a custom manager for your user model. If your user
# model defines username, email, is_staff, is_active, is_superuser, last_login, and date_joined

class CustomUserManager(BaseUserManager):

    def _create_user(self, username, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not username:
            raise ValueError("The given username must be set")
        email = self.normalize_email(email)
        # Lookup the real model class from the global app registry so this
        # manager method can be used in migrations. This is fine because
        # managers are by definition working on the real model.
        GlobalUserModel = apps.get_model(
            self.model._meta.app_label, self.model._meta.object_name
        )
        username = GlobalUserModel.normalize_username(username)
        user = self.model(username=username, email=email, **extra_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email=None, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(username, email, password, **extra_fields)

    def with_perm(
            self, perm, is_active=True, include_superusers=True, backend=None, obj=None
    ):
        if backend is None:
            backends = auth._get_backends(return_tuples=True)
            if len(backends) == 1:
                backend, _ = backends[0]
            else:
                raise ValueError(
                    "You have multiple authentication backends configured and "
                    "therefore must provide the `backend` argument."
                )
        elif not isinstance(backend, str):
            raise TypeError(
                "backend must be a dotted import path string (got %r)." % backend
            )
        else:
            backend = auth.load_backend(backend)
        if hasattr(backend, "with_perm"):
            return backend.with_perm(
                perm,
                is_active=is_active,
                include_superusers=include_superusers,
                obj=obj,
            )
        return self.none()


# The easiest way to construct a compliant custom user model is to inherit from AbstractBaseUser. AbstractBaseUser
# provides the core implementation of a User model, including hashed passwords and tokenized password resets. You
# must then provide some key implementation details: To make it easy to include Django’s permission framework into
# your own user class, Django provides PermissionsMixin. This is an abstract model you can include in the class
# hierarchy for your user model, giving you all the methods and database fields necessary to support Django’s
# permission model.

# custom user model
# PermissionsMixin is used to create group


class User(AbstractBaseUser, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _("username"),
        max_length=150,
        unique=True,
        help_text=_(
            "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
        ),
        validators=[username_validator],
        error_messages={
            "unique": _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_("first name"), max_length=150, blank=False, null=False)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    email = models.EmailField(_("email address"), null=False, blank=False, unique=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    is_ngt_admin = models.BooleanField(
        _("ngt admin status"),
        default=False,
        help_text=_("Designates whether the user should be treated as ngt admin")
    )
    is_ngt_user = models.BooleanField(
        _("ngt user status"),
        default=False,
        help_text=_("Designates whether the user should be treated as ngt user")
    )

    # we need to specify user manger here to create user and get objects from Manager
    objects = CustomUserManager()

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email"]


def app_upload_path(instance, filename):
    return '/'.join(['app/logo', filename])


def validate_file_size(value):
    filesize = value.size

    if filesize > 10485760:
        raise ValidationError("The maximum file size that can be uploaded is 10MB")
    else:
        return value


class Android_App(models.Model):
    id = models.AutoField(primary_key=True)
    App_name = models.CharField(max_length=256, null=False, blank=False)
    App_logo = models.FileField(upload_to=app_upload_path, validators=[validate_file_size], blank=True)
    App_link = models.URLField(null=False, blank=False)
    Points = models.PositiveIntegerField(null=False, blank=False)
    App_category = models.CharField(max_length=256, null=False, blank=False)
    Sub_category = models.CharField(max_length=256, null=False, blank=False)
    Created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='app_creator')
    Created_at = models.DateTimeField(default=timezone.now)
    Updated_at = models.DateTimeField(null=True, blank=True)
    Updated_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True,
                                   related_name='app_updator')


class Tasks(models.Model):
    id = models.AutoField(primary_key=True)
    Task_name = models.CharField(max_length=1000, null=False, blank=False)
    App = models.ForeignKey(Android_App, on_delete=models.CASCADE)
