from rest_framework import serializers
from .models import *


class CreateUserSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'first_name', 'is_ngt_user')
        extra_kwargs = {
            'password': {'write_only': True}
        }


class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model = Android_App
        fields = '__all__'


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields = '__all__'
