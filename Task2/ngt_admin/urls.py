from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path
from .views import *

urlpatterns = [
                  path('', home),
                  path('signin', signin, name='signin'),
                  path('products', add_app, name="add_product"),
                  path('homepage', view_products, name="homepage"),
                  path('edit/products/<int:pk>', edit_products, name='edit_product'),
                  path('signout', signout, name='signout'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
