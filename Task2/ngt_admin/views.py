from datetime import datetime
from django.conf import settings
from django.http import HttpResponse
from pytz import timezone
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from .serializers import *
from .models import *


# Create your views here.

def home(request):
    return redirect('/signin')


@api_view(['GET', 'POST'])
@permission_classes([AllowAny])
def signin(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        try:
            user = User.objects.get(username=username)
        except:
            return render(request, 'html/signin.html', {'messages': {"username": "User not found"}})
        try:
            user_data = authenticate(username=username, password=password)
            login(request, user_data)
        except:
            return render(request, 'html/signin.html', {'messages': {"password": "Wrong password"}})
        if user.is_ngt_admin:
            return redirect('/homepage')
        elif user.is_ngt_user:
            return redirect('/user/home')
    return render(request, 'html/signin.html')


@login_required
def signout(request):
    logout(request)
    return redirect('/signin')


@login_required
def add_app(request):
    if request.user.is_ngt_admin:
        if request.method == 'POST':
            data = request.POST.copy()
            # data['Created_at'] = datetime.datetime.now(timezone('Asia/Kolkata'))
            data['Created_by'] = request.user.id
            data['App_logo'] = request.FILES['App_logo']
            data['Points'] = int(request.POST['Points'].split()[0])
            serializer = AppSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                task_data = {
                    "Task_name": "{}: Upload a screenshot to complete the task".format(serializer.data['App_name']),
                    "App": serializer.data['id']
                }
                task_serializer = TaskSerializer(data=task_data)
                task_serializer.is_valid()
                task_serializer.save()
                return redirect('/homepage')
            else:
                return render(request, 'html/add_product.html', {"messages": error(serializer.errors)})
        else:
            return render(request, 'html/add_product.html')
    else:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)


@login_required
def edit_products(request, pk):
    if request.user.is_ngt_admin:
        data = Android_App.objects.get(id=pk)
        serializer = AppSerializer(data)
        res_data = serializer.data
        res_data['App_logo'] = settings.PROJECT_URL + serializer.data['App_logo']
        if request.method == 'POST':
            inp_data = request.POST.copy()
            inp_data['Created_at'] = data.Created_at
            inp_data['Created_by'] = request.user.id
            inp_data['Updated_at'] = timezone.now()
            inp_data['Updated_by'] = request.user.id
            if len(request.FILES) != 0:
                inp_data['App_logo'] = request.FILES['App_logo']
            else:
                del inp_data['App_logo']
            inp_data['Points'] = int(request.POST['Points'].split()[0])
            serializer = AppSerializer(data, data=inp_data)
            if serializer.is_valid():
                serializer.save()
                return redirect('/homepage')
            else:
                print(serializer.errors)
                return render(request, 'html/edit_product.html',
                              {"data": res_data, "messages": error(serializer.errors)})
        return render(request, 'html/edit_product.html', {"data": res_data})
    else:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)


@login_required
def view_products(request):
    if request.user.is_ngt_admin:
        products = Android_App.objects.all()
        serializer = AppSerializer(products, many=True)
        for i in serializer.data:
            i['App_logo'] = settings.PROJECT_URL + i['App_logo']
        return render(request, 'html/homepage.html', {"messages": serializer.data})
    elif request.user.is_ngt_user:
        return HttpResponse(status=status.HTTP_200_OK)


def error(data):
    error_list = {}
    for i in data:
        err_list = {}
        for j in data.get(i):
            err = {i: str(j)}
            error_list.update(err)
        # error_list.append(err_list)
    return error_list


def current_time():
    return datetime.now(timezone('Asia/Kolkata'))
