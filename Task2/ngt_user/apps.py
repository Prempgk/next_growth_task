from django.apps import AppConfig


class NgtUserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ngt_user'
