from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from ngt_admin.models import Tasks, Android_App, User, validate_file_size


# Create your models here.

def task_upload_path(instance, filename):
    return '/'.join(['task/completed', filename])


class TaskCompleted(models.Model):
    id = models.AutoField(primary_key=True)
    User_id = models.ForeignKey(User, on_delete=models.CASCADE)
    App = models.ForeignKey(Android_App, on_delete=models.CASCADE)
    Task = models.ForeignKey(Tasks, on_delete=models.CASCADE)
    Screenshot = models.FileField(upload_to=task_upload_path, validators=[validate_file_size])
    Completed_at = models.DateTimeField(null=False, blank=False, default=timezone.now)


class Points(models.Model):
    id = models.AutoField(primary_key=True)
    User_id = models.ForeignKey(User, on_delete=models.CASCADE)
    Points = models.PositiveBigIntegerField(null=False, blank=False)
