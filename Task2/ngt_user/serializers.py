from rest_framework import serializers
from .models import *


class CompletedTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskCompleted
        fields = '__all__'


class PointSerializer(serializers.ModelSerializer):
    class Meta:
        model = Points
        fields = '__all__'
