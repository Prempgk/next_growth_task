from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path
from .views import *

urlpatterns = [
                  path('signup', registration),
                  path('user/home', user_home),
                  path('user/task/<int:pk>', add_task, name='edit_product'),
                  path('user/profile', user_profile),
                  path('user/points', user_points),
                  path('user/task/view', user_task),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
