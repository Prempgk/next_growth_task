from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from ngt_admin.models import Android_App, Tasks
from ngt_admin.serializers import AppSerializer, CreateUserSerializer
from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny
from .serializers import *
from .models import *
from ngt_admin.views import error


# Create your views here.


@api_view(['GET', 'POST'])
@permission_classes([AllowAny])
def registration(request):
    if request.method == 'POST':
        a = request.POST.copy()
        a['is_ngt_user'] = True
        a['is_active'] = True
        serializer = CreateUserSerializer(data=a)
        if serializer.is_valid():
            serializer.save()
            data = {
                "User_id": serializer.data['id'],
                "Points": 0
            }
            point_serializer = PointSerializer(data=data)
            point_serializer.is_valid()
            point_serializer.save()
            return redirect('/signin')
        else:
            return render(request, 'html/signup.html', {"messages": error(serializer.errors)})
    return render(request, 'html/signup.html')


@login_required
def user_home(request):
    if not request.user.is_ngt_user:
        return HttpResponse(status=status.HTTP_403_FORBIDDEN)
    products = Android_App.objects.all()
    serializer = AppSerializer(products, many=True)
    for i in serializer.data:
        i['App_logo'] = settings.PROJECT_URL + i['App_logo']
    return render(request, 'html/user_home.html', {"messages": serializer.data, "user": request.user.first_name})


@login_required
def add_task(request, pk):
    products = Android_App.objects.get(id=pk)
    task_model = products.taskcompleted_set.all().filter(User_id=request.user.id)
    serializer = AppSerializer(products)
    res_data = serializer.data
    res_data['App_logo'] = settings.PROJECT_URL + serializer.data['App_logo']
    if request.method == 'POST':
        inp_data = request.POST.copy()
        print(request.FILES)
        inp_data['Screenshot'] = request.FILES['Screenshot']
        inp_data['User_id'] = request.user.id
        app = Android_App.objects.get(id=inp_data['App'])
        inp_data['Task'] = app.tasks_set.all()[0].id
        serializer = CompletedTaskSerializer(data=inp_data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            point_model = Points.objects.get(User_id=request.user.id)
            point_model.User_id = request.user
            point_model.Points = point_model.Points + app.Points
            point_model.save()
            return redirect('/user/home')
    if len(task_model) != 0:
        return render(request, 'html/task_upload.html', {"messages": res_data,
                                                         "user": request.user.first_name,
                                                         "task": settings.PROJECT_URL + '/media/' + str(
                                                             task_model[0].Screenshot)})
    return render(request, 'html/task_upload.html', {"messages": res_data, "user": request.user.first_name})


@login_required
def user_profile(request):
    return render(request, 'html/user_profile.html', {"user": request.user.first_name, "messages": request.user})


@login_required
def user_points(request):
    return render(request, 'html/user_profile.html', {"user": request.user.first_name,
                                                      "points": request.user.points_set.all()[0].Points})


@login_required
def user_task(request):
    tasks = Tasks.objects.all()
    completed = TaskCompleted.objects.filter(User_id=request.user.id)
    com_serializer = CompletedTaskSerializer(completed, many=True)
    complete_data = com_serializer.data
    for i in complete_data:
        task_data = tasks.get(id=i['Task'])
        i['Task_name'] = task_data.Task_name
    return render(request, 'html/user_profile.html', {"user": request.user.first_name,
                                                      "tasks": [tasks, complete_data]})
