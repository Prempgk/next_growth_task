# 💻 Application Overview


## Data model

The application contains the following core features:

- User - can have these roles:
  #### Notable Features:
  - `USERS` - can:
    - ✅ User have signup, login & logout
    - ✅ User can view the applications in homepage
    - ✅ User can complete the task by adding a screenshot of an application.
    - ✅ User can earn points for completing task for particular applications.
    - ✅ User can view their own profile.
    - ✅ User can view tha tasks.

  - `Admin` - can:
    - Application Management
    - Login, Logout.
    
     
## Get Started

Prerequisites:

- Python v3.10.4
- Django v4.1.5
- HTML 5
- Css
- Java script

To set up the app execute the following guide.

### Setup, Installation and Run

To run the app on your local machine, you need Python 3+, installed on your computer. Follow all the steps to run this project.

1.  Create virtual environment:
```bash
virtualenv env
```
2.  Activate virtual environment:
```bash
On Linux - source env/bin/activate
On Windows - env/Scripts/activate
```
3. Firstly you need to clone or download my project from gitlab repositories:
```bash
git clone https://gitlab.com/Prempgk/next_growth_task.git
```

4. Then enter the corresponding directory:
```bash
cd Task2
```
5. Install dependencies
```bash
  pip install -r requirements.txt
``` 

6. Run local server, and DONE!
```python
  python manage.py runserver
  Linux or Mac - python3 manage.py runserver
```
7.  App in the development mode.\
Open [http://127.0.0.1:8000](http://127.0.0.1:8000) to view it in the browser.

