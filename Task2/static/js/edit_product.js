let hbtn = window.location.pathname.split("/")
const btn_div = document.querySelector('.actbt').getElementsByTagName('a')
var currentPage = hbtn[hbtn.length - 1];
for(i=0;i<btn_div.length;i++){
  var lb = btn_div[i].href.split("/");
  if(lb[lb.length-1] == currentPage) {
   btn_div[i].className = "current d-block my-2 d-flex align-items-center";
  }
}

var inputs, index;
var selects;
var a;
function field_validation() {
  a = 0;
  let isValid = true;
  inputs = document.getElementsByTagName("input");
  selects = document.getElementsByTagName("select");
  for (index = 0; index < inputs.length; ++index) {
    let changedInput = inputs[index];
    if (changedInput.value === null) {
      isValid = false;
      break;
    }
  }
  for (index = 0; index < selects.length; ++index) {
    let changedInput = selects[index];
    if (changedInput.value === null) {
      isValid = false;
      break;
    }
  }
  var btn = document.getElementById("submit_btn");
  if (isValid === true) {
    btn.style.display = "block";
  } else {
    btn.style.display = "none";
  }
}

var category_list = {
  Education: [
    "History",
    "Geography",
    "Maths",
    "Physics",
    "Tamil",
    "English",
    "Others",
  ],
  Dictionary: [
    "Tamil Dictionary",
    "English Dictionary",
    "Hindi Dictionary",
    "Malayalam Dictionary",
    "Others",
  ],
  Entertainment: [
    "Sports",
    "Movies",
    "Web series",
    "Drama",
    "Dance",
    "Shows",
    "Magic",
    "Comedy",
    "Games",
    "Others",
  ],
  Business: ["Banking", "Trading", "Administration", "Others"],
  Communication: ["Chat", "Vidoe call", "Dating", "Social Media"],
};

window.onload = function () {
  let category = document.getElementById("category");
  let subcategory = document.getElementById("subcategory");
  let ct = document.getElementById("cid");
  let ct_value = ct.innerHTML;
  let sc = document.getElementById("scid");
  let sc_value = sc.innerHTML;
  for (var x in category_list) {
    category.options[category.options.length] = new Option(x, x);
  }
  if (ct_value in category_list) {
    category_list_keys = Object.keys(category_list);
    ct_index = category_list_keys.indexOf(ct_value);
    category.selectedIndex = ct_index;
    var z = category_list[ct_value];
    for (var i = 0; i < z.length; i++) {
      subcategory.options[subcategory.options.length] = new Option(z[i], z[i]);
    }
  }
  sc_index = category_list[ct_value].indexOf(sc_value);
  subcategory.selectedIndex = sc_index;

  category.onchange = function () {
    //empty Chapters- and Topics- dropdowns
    subcategory.length = 1;
    //display correct values
    field_validation();
    var z = category_list[category.value];
    for (var i = 0; i < z.length; i++) {
      subcategory.options[subcategory.options.length] = new Option(z[i], z[i]);
    }
  };
};

function point_fun() {
  var pnt = document.getElementById("point_id");
  c = pnt.value;
  d = c.split(" ");
  if (isNumeric(d[0]) === true) {
    b = d[0] + " " + "POINTS";
  } else {
    b = "0 POINTS";
  }
  pnt.value = b;
}

function isNumeric(str) {
  if (typeof str != "string") return false; // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str))
  ); // ...and ensure strings of whitespace fail
}

var loadFile = function (event) {
  var image = document.getElementById("output");
  image.src = URL.createObjectURL(event.target.files[0]);
  image.height = "200";
  var lab_img = document.getElementById("logo");
  var lab_i = document.getElementById("lab_i");
  lab_img.remove();
  lab_i.remove();
  var dab_tag = document.getElementById("lab_img");
  dab_tag.innerText = "Please click here to change image";
};

function hrefFunction() {
  window.location.href = "/homepage";
}

//Function demonstrating the Replace Method

function replaceFunction() {
  window.location.href = "/products";
}
function signoutFunction() {
  window.location.href = "/signout";
}

function error_fun() {
  const err_msg = document.getElementById("err").innerHTML;
  var a = err_msg.replaceAll(`'`, `"`);
  const b = JSON.parse(a);
  for (i in b) {
    const res_err = document.getElementById(i);
    const err_field = res_err.parentElement;
    res_err.style.border = "1px solid red";
    const error = err_field.querySelector("small");
    error.textContent = b[i];
  }
}
